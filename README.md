# README zu Straßenfeste Berlin

Diese App ermöglicht es dem Nutzer sich über aktuelle Straßen- und Volksfeste in Berlin zu informieren. Es kann ganz einfach nach bekannten Straßenfesten gesucht werden. Oder man schaut sich an, welche Veranstaltung demnächst in seinem Bezirk statt findet.
Zu jeder Veranstaltung können sich natürlich weitere Informationen anzeigen lassen oder sich direkt über GoogleMaps anzeigen lassen wo die Veranstaltung genau stattfindet.

Aktuell verfügbare Version 1.0.


# Setup

1. Download bzw. clone von [Sören Ziera](https://soerenz@bitbucket.org/tms_ws16_08/strassenfestberlin/downloads)

2. Per Android Studio ein bereits existierendes Projekt öffnen. Das eben geladene  Projekt auswählen und den automatischen Grade Build durchlaufen lassen.
Falls du über Android Studio das Repository eingebunden hast entfällt natürlich dieser Punkt.

3. Erforderlich mindestens die Android SDK Version 17.

5. Starten der App über den Android Studio Emulator (oder einen eigenen) oder über das eigene Android Device (bitte auf die SDK Version bzw. das API-Level achten)

# Permissions

* Internetberechtigung

```
#!

<uses-permission android:name="android.permission.INTERNET" />

```
* Zugriff auf Netzwerkstatus

```
#!

  <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
```


# Guideline

Nach dem Start der App gelangst du in die SearchView in der nach Straßenfesten gesucht werden kann. 

![startscreen.jpg](https://bitbucket.org/repo/BL86Kj/images/945722778-startscreen.jpg)

**Achtung beim ersten Start der App müssen die Daten erstmals aktualisiert werden über das OptionsMenu oben rechts in der Actionbar**

Die Daten können jederzeit aktualisiert werden.

![refreshBtn.jpg](https://bitbucket.org/repo/BL86Kj/images/1153267118-refreshBtn.jpg)

Wenn du einen Suchbegriff in die SearchView eintippst passen sich die Suchergebnisse dynamisch an.

![searchDistrict.jpg](https://bitbucket.org/repo/BL86Kj/images/1807235324-searchDistrict.jpg)  ![searchZipCode.jpg](https://bitbucket.org/repo/BL86Kj/images/4044265941-searchZipCode.jpg)

Wenn du eine Veranstaltung aus der ListView auswählst kommst du auf die Detailansicht dieser Veranstaltung. Dort sind weiter Informationen und auch weiterführende Links enthalten z.B. Telefon, E-Mail, oder du kannst dir durch ein klick auf die Adresse den Ort in GoogleMaps anzeigen lassen.

![eventDetails.jpg](https://bitbucket.org/repo/BL86Kj/images/2114568377-eventDetails.jpg)

Über den Navigationdrawer links-oben in der Actionbar rufst du das Menü auf.
Darin kannst du zwischen Suche und AppInfo hin- und herschalten.

![navdraw.jpg](https://bitbucket.org/repo/BL86Kj/images/436700978-navdraw.jpg)

In der AppInfo findest du die derzeitige Version der App und die Kontaktdaten des Authors.

![appInfo.jpg](https://bitbucket.org/repo/BL86Kj/images/3316476438-appInfo.jpg)


# Erweiterte Einstellungen

Derzeit gibt es nur die Option die Daten zu aktualisieren. Im weiteren Entwicklungsprozess kommen noch einige Optionen und Einstellungen hinzu.


# Geplante Funktionen

* Favoritenliste in der vorher markierte Veranstaltungen aufgelistet werden.
* Aktuelle Veranstaltungen der nächsten 7 Tage anzeigen
* Filterfunktionen für die ListView (nach Datumsangaben)
* Expandable Listview mit zwischen Überschriften der Monate.
* Kalenderfunktion in der die Favorisierten Veranstaltungen auch in die Kalenderapo eingetragen werden können.



# Autor / Support

* Repo und App Ersteller ist [Sören](https://bitbucket.org/soerenz/)
* Erstellt wurde dieses Projekt im Zusammenhang mit einer Belegarbeit an der HTW-Berlin