package de.htw_berlin.tms.soerenz.strassenfestberlin;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Event Klasse zur Weiterverarbeitung der Xml-Daten, die in das Event Objekt geschrieben werden.
 * Besteht nur aus Attributen und deren Getter und Setter.
 * Damit eine Event Objekt auch zwischen Fragements geschickt werden kann wurde das Parcelable
 * Interface implementiert
 */

public class Event implements Parcelable {

    private int mId;
    private String mDistrict;
    private String mDescription;
    private String mStreet;
    private int mZip;
    private String mBeginDate;
    private String mEndDate;
    private String mOpeningTime;
    private String mOrganizer;
    private String mMailOrganizer;
    private String mWebUrl;
    private String mNotice;

    public Event() { }

    public int getId() {
        return mId;
    }

    public String getDistrict() {
        return mDistrict;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getStreet() {
        return mStreet;
    }

    public int getZip() {
        return mZip;
    }

    public String getBeginDate() {
        return mBeginDate;
    }

    public String getEndDate() {
        return mEndDate;
    }

    public String getOpeningTime() {
        return mOpeningTime;
    }

    public String getOrganizer() {
        return mOrganizer;
    }

    public String getMailOrganizer() {
        return mMailOrganizer;
    }

    public String getWebUrl() {
        return mWebUrl;
    }

    public String getNotice() {
        return mNotice;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public void setDistrict(String district) {
        this.mDistrict = district;
    }

    public void setStreet(String street) {
        this.mStreet = street;
    }

    public void setZip(int zip) {
        this.mZip = zip;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public void setBeginDate(String beginDate) {
        this.mBeginDate = beginDate;
    }

    public void setEndDate(String endDate) {
        this.mEndDate = endDate;
    }

    public void setOpeningTime(String openingTime) {
        this.mOpeningTime = openingTime;
    }

    public void setOrganizer(String organizer) {
        this.mOrganizer = organizer;
    }

    public void setMailOrganizer(String mailOrganizer) {
        this.mMailOrganizer = mailOrganizer;
    }

    public void setWebUrl(String webUrl) {
        this.mWebUrl = webUrl;
    }

    public void setNotice(String notice) {
        this.mNotice = notice;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mDistrict);
        dest.writeString(mDescription);
        dest.writeString(mStreet);
        dest.writeInt(mZip);
        dest.writeString(mBeginDate);
        dest.writeString(mEndDate);
        dest.writeString(mOpeningTime);
        dest.writeString(mOrganizer);
        dest.writeString(mMailOrganizer);
        dest.writeString(mWebUrl);
        dest.writeString(mNotice);
    }

    private Event(Parcel in) {
        this.mId = in.readInt();
        this.mDistrict = in.readString();
        this.mDescription = in.readString();
        this.mStreet = in.readString();
        this.mZip = in.readInt();
        this.mBeginDate = in.readString();
        this.mEndDate = in.readString();
        this.mOpeningTime = in.readString();
        this.mOrganizer = in.readString();
        this.mMailOrganizer = in.readString();
        this.mWebUrl = in.readString();
        this.mNotice = in.readString();
    }

    public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {

        @Override
        public Event createFromParcel(Parcel source) {
            return new Event(source);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
}
