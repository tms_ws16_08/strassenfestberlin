package de.htw_berlin.tms.soerenz.strassenfestberlin;

import android.app.FragmentManager;
import android.content.res.Configuration;
import android.os.PersistableBundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class SearchActivity extends AppCompatActivity {

    private ActionBarDrawerToggle mABDrawerToggle;
    private DrawerLayout mDrawerLayout;

    //Fragments
    private EventListFragment eventListFragment;
    private AppInfoFragment appInfoFragment;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ListView mDrawerListView;
        ArrayAdapter<String> mDrawerListAdapter;
        String[] mDrawerListArray;

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerListView = (ListView) findViewById(R.id.left_drawer);
        mDrawerListArray = new String[]{
                getResources().getString(R.string.generalSearch),
                getResources().getString(R.string.appInfo)
        };
        mDrawerListAdapter = new ArrayAdapter<>(
                this,
                R.layout.drawer_list_item,
                R.id.list_item_drawer,
                mDrawerListArray
        );
        mDrawerListView.setAdapter(mDrawerListAdapter);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        //Set the list's click listener
        mDrawerListView.setOnItemClickListener(new DrawerItemClickListener());

        mABDrawerToggle = new ActionBarDrawerToggle(
                SearchActivity.this,
                mDrawerLayout,
               // R.drawable.ic_drawer,
                R.string.drawer_open,
                R.string.drawer_close);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_drawer);
        }

        eventListFragment = new EventListFragment(); //(EventListFragment) Fragment.instantiate(this, EventListFragment.class.getName());
        appInfoFragment = new AppInfoFragment();

        fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().add(R.id.content_frame, eventListFragment).commit();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        mABDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);

        mABDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return mABDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    /** Swaps fragment in the main content view */
    private void selectItem(int position) {

        switch (position) {
            //General Seach
            case 0:
                fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, eventListFragment)
                        .addToBackStack(null).commit();
                mDrawerLayout.closeDrawers();
                break;
            //App Info
            case 1:
                fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().
                        replace(R.id.content_frame, appInfoFragment)
                        .addToBackStack(null).commit();
                mDrawerLayout.closeDrawers();

            default: break;
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(position);
        }
    }





}


