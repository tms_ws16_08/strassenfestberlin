package de.htw_berlin.tms.soerenz.strassenfestberlin;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Fragment, das eine Detaillierte Ansicht einer Veranstaltung anzeigt.
 */
public class EventDetailsFragment extends Fragment {

    private TextView mAddressTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentLayout = inflater.inflate(R.layout.fragment_event_details, container, false);

        TextView mEventNameTextView;
        TextView mDateTextView;
        TextView mNoticeTextView;
        TextView mContactOrganizerTextView;
        TextView mMailOrganizerTextView;
        TextView mUrlOrganizerTextView;
        TextView mOpeningTimeTextView;
        ImageView mImageMaps;

        mEventNameTextView = (TextView) fragmentLayout.findViewById(R.id.tvEventName);
        mAddressTextView = (TextView) fragmentLayout.findViewById(R.id.tvEventAddress);
        mDateTextView = (TextView) fragmentLayout.findViewById(R.id.tvEventDate);
        mNoticeTextView = (TextView) fragmentLayout.findViewById(R.id.tvEventNotice);
        mContactOrganizerTextView = (TextView) fragmentLayout.findViewById(R.id.tvEventOrganizer);
        mMailOrganizerTextView = (TextView) fragmentLayout.findViewById(R.id.tvEventMailOrganizer);
        mUrlOrganizerTextView = (TextView) fragmentLayout.findViewById(R.id.tvEventUrlOrganizer);
        mImageMaps = (ImageView) fragmentLayout.findViewById(R.id.imageButton_maps);
        mOpeningTimeTextView = (TextView) fragmentLayout.findViewById(R.id.tvOpeningTime);

        Bundle bundle = getArguments();
        Event event = bundle.getParcelable("event");

        if (event != null) {
            mEventNameTextView.setText(event.getDescription());
            mAddressTextView.setText(event.getStreet() + ", " + event.getZip());
            mDateTextView.setText(event.getBeginDate() + " - " + event.getEndDate());
            mNoticeTextView.setText(event.getNotice());
            mContactOrganizerTextView.setText(event.getOrganizer());
            mMailOrganizerTextView.setText(event.getMailOrganizer());
            mOpeningTimeTextView.setText(event.getOpeningTime());
            mUrlOrganizerTextView.setText(event.getWebUrl());
        }


        mAddressTextView.setOnClickListener(addressClickListener);
        mImageMaps.setOnClickListener(addressClickListener);

        return fragmentLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private final View.OnClickListener addressClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String map = "http://maps.google.co.in/maps?q=" + mAddressTextView.getText().toString();
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
            startActivity(i);
        }
    };
}
