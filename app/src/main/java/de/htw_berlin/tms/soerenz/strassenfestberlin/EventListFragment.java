package de.htw_berlin.tms.soerenz.strassenfestberlin;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Fragment, welches die Strassenfeste in einer ListView anzeigen soll.
 */
public class EventListFragment extends Fragment {

    private ArrayAdapter<String> mEventlistAdapter;
    private String[] mEventlistArray = {""};
    private final String mFilename = "StrBer.xml";
    private ListView eventlistListView;
    private List<Event> mAllEvents;
    private TextView mSearchFeedback;

    public EventListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Menü bekannt geben, dadurch kann unser Fragment Menü-Events verarbeiten
        setHasOptionsMenu(true);
        mAllEvents = new ArrayList<>();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_options, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Wir prüfen, ob Menü-Element mit der ID "action_daten_aktualisieren"
        // ausgewählt wurde und geben eine Meldung aus
        int id = item.getItemId();
        if (id == R.id.actionRefreshData) {

            // Erzeugen einer Instanz von HoleDatenTask und starten des asynchronen Tasks
            DownloadFileTask downloadFileTask = new DownloadFileTask();
            downloadFileTask.execute("Event");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_eventlist, container, false);

        String[][] xmlEventDataMatrix;
        SearchView mSearchView;
        List<String> eventlist;

        eventlistListView = (ListView) rootView.findViewById(R.id.listview_eventlist);
        mSearchFeedback = (TextView) rootView.findViewById(R.id.tv_search_feedback);
        mSearchFeedback.setText(R.string.tvSearchFeedbackShowAll);

        String xmlFile = readFile(mFilename);
        if (xmlFile != null) {
            xmlEventDataMatrix = readXmlEventData(xmlFile);
            mEventlistArray = createListEntries(xmlEventDataMatrix);
        } else {
            mEventlistArray[0] = getText(R.string.dataRefreshRequest).toString();
        }

        eventlist = new ArrayList<>(Arrays.asList(mEventlistArray));
        mEventlistAdapter =
                new ArrayAdapter<>(
                        getActivity(), // Die aktuelle Umgebung (diese Activity)
                        R.layout.event_list_item, // ID der XML-Layout Datei
                        R.id.list_item_eventlist, // ID des TextViews
                        eventlist); // Beispieldaten in einer ArrayList

        eventlistListView.setAdapter(mEventlistAdapter);

        mSearchView = (SearchView) rootView.findViewById(R.id.searchview_eventlist);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                mEventlistAdapter.getFilter().filter(newText);
                if (newText.equals("")) {
                    mSearchFeedback.setText(R.string.tvSearchFeedbackShowAll);
                } else {
                    mSearchFeedback.setText(R.string.tvSeachFeedbackShowResults);
                }
                return false;
            }
        });
        eventlistListView.setOnItemClickListener(listItemClickListener);


        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private final AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            String currentString  = eventlistListView.getItemAtPosition(position).toString();
            StringTokenizer tokens = new StringTokenizer(currentString, "\n");
            String name = tokens.nextToken();
            int currentPosition = 0;
            for (int i = 0; i < mAllEvents.size(); i++) {
                if (name.equals(mAllEvents.get(i).getDescription())) {
                    currentPosition = i;
                }
            }


            Bundle bundle = new Bundle();
            bundle.putParcelable("event", mAllEvents.get(currentPosition)); // use as per your need
            Fragment eventDetailsFragment = new EventDetailsFragment();
            eventDetailsFragment.setArguments(bundle);
            getFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, eventDetailsFragment).addToBackStack(null).commit();
        }

    };


    private String readFile(String filename) {

        String line;
        String resultString = null;
        FileInputStream fileinputStream;

        Context ctx = getActivity().getApplicationContext();
        try {
            fileinputStream = ctx.openFileInput(filename);
            InputStreamReader isr = new InputStreamReader(fileinputStream);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            resultString = sb.toString();
            if (resultString.length() == 0) { // Keine Eventdaten ausgelesen, Abbruch
                return null;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
            Log.e("IOException", " " + ex);
        }

        return resultString;

    }

    private String[][] readXmlEventData(String xmlString) {

        final String logTag = "readXmlEventDate Method";
        String[][] mAllEventDataArray;

        Document doc;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xmlString));
            doc = db.parse(is);
        } catch (ParserConfigurationException e) {
            Log.e(logTag, "Error: " + e.getMessage());
            return null;
        } catch (SAXException e) {
            Log.e(logTag, "Error: " + e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e(logTag, "Error: " + e.getMessage());
            return null;
        }

        Element xmlEventData = doc.getDocumentElement();
        NodeList eventNodeList = xmlEventData.getElementsByTagName("item"); //row im orginal

        int numbersOfEvents = eventNodeList.getLength() - 1;
        int numbersOfEventAttributes = eventNodeList.item(0).getChildNodes().getLength();
        mAllEventDataArray = new String[numbersOfEvents][numbersOfEventAttributes];

        Node eventAttribute;
        String eventAttributeValue = "";
        for (int i = 0; i < numbersOfEvents; i++) {
            NodeList eventAttributeNodeList = eventNodeList.item(i).getChildNodes();
            for (int j = 0; j < numbersOfEventAttributes; j++) {
                if (eventAttributeNodeList.item(j) != null) {
                    eventAttribute = eventAttributeNodeList.item(j);

                    if (eventAttribute.getFirstChild() != null) {
                        eventAttributeValue = eventAttribute.getFirstChild().getNodeValue();
                    }
                    mAllEventDataArray[i][j] = eventAttributeValue;
                }
            }
        }
        return mAllEventDataArray;
    }

    private String[] createListEntries(String[][] xmlData) {
        String[] listEntries = new String[xmlData.length];
        final int id = 0;
        final int district = 1;
        final int description = 2;
        final int street = 3;
        final int zip = 4;
        final int beginDate = 5;
        final int endDate = 6;
        final int openingTime = 7;
        final int organizer = 8;
        final int mail = 9;
        final int url = 10;
        final int notice = 11;

        for (int i = 0; i < xmlData.length; i++) {

            Event newEvent = new Event();
            newEvent.setId(Integer.parseInt(xmlData[i][id]));
            newEvent.setDistrict(xmlData[i][district]);
            newEvent.setDescription(xmlData[i][description]);
            newEvent.setStreet(xmlData[i][street]);
            newEvent.setZip(Integer.parseInt(xmlData[i][zip]));
            newEvent.setBeginDate(xmlData[i][beginDate]);
            newEvent.setEndDate(xmlData[i][endDate]);
            newEvent.setOpeningTime(xmlData[i][openingTime]);
            newEvent.setOrganizer(xmlData[i][organizer]);
            newEvent.setMailOrganizer(xmlData[i][mail]);
            newEvent.setWebUrl(xmlData[i][url]);
            newEvent.setNotice(xmlData[i][notice]);


            mAllEvents.add(newEvent);

            listEntries[i] =  newEvent.getDescription() + "\n "; // Bezeichnung
            listEntries[i] += newEvent.getDistrict() + ", " + newEvent.getZip() + "\n "; // Bezirk, PLZ
            listEntries[i] += newEvent.getBeginDate() + " bis " + newEvent.getEndDate(); // Datum
        }
        return listEntries;
    }


    // Innere Klasse DownloadFileTask führt den asynchronen Task auf eigenem Arbeitsthread aus
    public class DownloadFileTask extends AsyncTask<String, Integer, String[]> {

        private final String LOG_TAG = DownloadFileTask.class.getSimpleName();
        FileOutputStream outputStream;
        final Context ctx = getActivity().getApplicationContext();

        @Override
        protected String[] doInBackground(String... strings) {

            if (strings.length == 0) { // Keine Eingangsparameter erhalten, daher Abbruch
                return null;
            }
            // Anfrage-Url von data.berlin.de zu Straßen- und Volksfesten
            final String urlParameter = "http://www.berlin.de/sen/wirtschaft/service/maerkte-feste/strassen-volksfeste/index.php/index/all.xml?q&bezirk=--%20Alles%20--&von_from&von_to&bis_from&bis_to&ipp=100&order=von";
            Log.v(LOG_TAG, "Zusammengesetzter Anfrage-String: " + urlParameter);

            // Die URL-Verbindung und der BufferedReader, werden im finally-Block geschlossen
            HttpURLConnection httpURLConnection = null;
            BufferedReader bufferedReader = null;

            // In diesen String speichern wir die Eventdaten im XML-Format
            String eventDataXmlStringDownload = "";


            try {
                URL url = new URL(urlParameter);

                // Aufbau der Verbindung zu data.berlin.de
                httpURLConnection = (HttpURLConnection) url.openConnection();

                InputStream inputStream = httpURLConnection.getInputStream();

                if (inputStream == null) { // Keinen Eventdaten-Stream erhalten, daher Abbruch
                    return null;
                }
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;


                while ((line = bufferedReader.readLine()) != null) {
                    eventDataXmlStringDownload += line + "\n";

                }
                if (eventDataXmlStringDownload.length() == 0) { // Keine Eventdaten ausgelesen, Abbruch
                    return null;
                }

                outputStream = ctx.openFileOutput(mFilename, Context.MODE_PRIVATE);
                outputStream.write(eventDataXmlStringDownload.getBytes());
                outputStream.close();

            } catch (IOException e) { // Beim Holen der Daten trat ein Fehler auf, daher Abbruch
                Log.e(LOG_TAG, "Error ", e);
                return null;
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }

            return createListEntries(readXmlEventData(eventDataXmlStringDownload));
        }

        @Override
        protected void onProgressUpdate(Integer... values) {}

        @Override
        protected void onPostExecute(String[] strings) {

            // Wir löschen den Inhalt des ArrayAdapters und fügen den neuen Inhalt ein
            // Der neue Inhalt ist der Rückgabewert von doInBackground(String...) also
            // der StringArray gefüllt mit Beispieldaten
            if (strings != null) {
                mEventlistArray = strings;
                mEventlistAdapter.clear();
                for (String eventString : strings) {
                    mEventlistAdapter.add(eventString);
                }
            }
        }
    }


}
